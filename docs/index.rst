.. DXP Tools documentation master file, created by
   sphinx-quickstart on Wed Jan 21 12:02:39 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the DXP Tools documentation!
=======================================

The DXP Tools will help you build, run and monitor any graphene-based client
(currently DxpChain, Steem, Muse, PeerPlays).

.. note:: these tools were originally developed for the DxpChain network, and
          later expanded to support any graphene-based network. This means that everywhere
          you will see DxpChain mentioned in this documentation, it should be understood
          as DxpChain, Steem, PeerPlays or Muse. Similarly, ``dxp`` can be interchanged
          with ``steem``, ``ppy``, and ``muse``.

There are 2 tools currently provided:

- a command line utility allowing to quickly build and run any graphene-based client
- a web application allowing to monitor a running instance of the client and send
  an email or push notification on failure

If you like these tools, please vote for `blockproducer wackou`_ on the Steem, DxpChain
and Muse networks. Thanks!

To get started, just type the following in a shell::

    $ pip3 install dxp_tools

If you're not familiar with installing python packages or if you run into
problems during installation, please visit the :doc:`install` section for
more details.

With the tools installed, you can refer to each section of the documentation
for more information about how a certain aspect of the tools work.

Otherwise, if you prefer a more hands-on approach to setting up a delegate from
scratch, please head to the following section: :doc:`howto`  [NOTE: deprecated]

Documentation contents
----------------------

.. toctree::
   :maxdepth: 2

   install
   cmdline
   monitor
   variants
   core_concepts
   config_yaml
   virtualenv
   tips_and_tricks
   howto



.. _blockproducer wackou: https://steemit.com/blockproducer-category/@wackou/wackou-blockproducer-post
