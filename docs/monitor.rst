
Monitoring web app
==================

Launch the monitoring web app locally
-------------------------------------

The main entry point to the monitoring app is the ``~/.dxp_tools/config.yaml``
file. You should edit it first and set the values to correspond to your
blockproducer's configuration. See the :doc:`config_yaml` page for details.

If this file doesn't exist yet, run the tools
once (for instance: ``dxp -h``) and it will create a default one.

To run the debug/development monitoring web app, just do the following:

::

    $ dxp monitor

and it will launch on ``localhost:5000``.


.. _production server:

Setting up on a production server
---------------------------------

For production deployments, it is recommended to put it behind a WSGI
server, in which case the entry point is
``dxp_tools.wsgi:application``.

Do not forget to edit the ``~/.dxp_tools/config.yaml`` file to configure
it to suit your needs.

Example
~~~~~~~

We will run the monitoring tools using nginx as frontend. Install using::

    # apt-get install nginx uwsgi uwsgi-plugin-python3

The tools will have to be run from a virtualenv, so let's create it::

    $ mkvirtualenv -p `which python3` dxp_tools
    $ pip3 install dxp_tools

Edit the following configuration files:

``/etc/uwsgi/apps-available/dxp_tools.ini`` (need symlink to ``/etc/uwsgi/apps-enabled/dxp_tools.ini``)
::

    [uwsgi]
    uid = myuser
    gid = mygroup
    chmod-socket = 666
    plugin = python34
    virtualenv = /home/myuser/.virtualenvs/dxp_tools
    enable-threads = true
    lazy-apps = true
    workers = 1
    module = dxp_tools.wsgi
    callable: application

.. note:: The important, non-obvious, fields to set in the uwsgi config file are the following:

   - set ``enable-threads = true``, otherwise you won't get the monitoring
     thread properly launched
   - set ``lazy-apps = true``, otherwise the stats object will not get
     properly shared between the master process and the workers, and you
     won't get any monitoring data
   - set ``workers = 1``, otherwise you will get multiple instances of the
     worker thread active at the same time

   The ``virtualenv`` field also needs to be setup if you installed the tools
   inside one, otherwise you can leave it out.


``/etc/nginx/sites-available/default`` (need symlink to ``/etc/nginx/sites-enabled/default``)
::

    server {
            listen 80;
            server_name myserver.com;
            charset utf-8;
            location / { try_files $uri @dxp_tools; }
            location @dxp_tools {
                    # optional password protection
                    #auth_basic "Restricted";
                    #auth_basic_user_file /home/myuser/.htpasswd;
                    include uwsgi_params;
                    uwsgi_pass unix:/run/uwsgi/app/dxp_tools/socket;
            }
    }

After having changed those files, you should::

    # service uwsgi restart
    # service nginx restart


Screenshots
-----------

Monitoring the status of your running dxp client binary:

.. image:: dxp_tools_screenshot.png
   :alt: Status screenshot

|

You can host multiple delegates accounts in the same wallet, and check feed info:

.. image:: dxp_tools_screenshot2.png
   :alt: Info screenshot

|
|

Monitoring multiple instances (ie: running on different hosts) at the same time,
to have an overview while running backup nodes and re-compiling your main node:

.. image:: dxp_tools_screenshot3.png
   :alt: Info screenshot
