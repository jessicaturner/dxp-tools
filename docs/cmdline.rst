
Command-line tools
==================

just run the ``dxp`` script with the command you want to execute:

::

    $ dxp -h
    usage: dxp [-h] [-p PIDFILE] [-f]
               {version,clean_homedir,clean,build,build_gui,run,run_cli,run_gui,list,monitor,deploy,deploy_node,truncate,feed_publish,feed_fetch,install_boost}
               [environment] [args [args ...]]
    
    following commands are available:
      - version                : show version of the tools
      - clean_homedir          : clean home directory. WARNING: this will delete your wallet!
      - save_blockchain_dir    : save a snapshot of the current state of the blockchain
      - restore_blockchain_dir : restore a snapshot of the current state of the blockchain
      - clean                  : clean build directory
      - build                  : update and build dxp client
      - build_gui              : update and build dxp gui client
      - run                    : run latest compiled dxp client, or the one with the given hash or tag
      - run_cli                : run latest compiled dxp cli wallet
      - run_gui                : run latest compiled dxp gui client
      - list                   : list installed dxp client binaries
      - monitor                : run the monitoring web app
      - deploy                 : deploy built binaries to a remote server
      - deploy_node            : full deploy of a seed or blockproducer node on given ip address. Needs ssh root access
      - truncate               : download, compile and install the specified boost version
      - feed_publish           : fetch all prices from feed sources and publishes them
      - feed_fetch             : fetch all prices from feed sources
      - install_boost          : download, compile and install the specified boost version
        
    Examples:
      $ dxp build                 # build the latest dxp client by default
      $ dxp build v0.4.27         # build specific version
      $ dxp build ppy-dev v0.1.8  # build a specific client/version
      $ dxp run                   # run the latest compiled client by default
      $ dxp run seed-test         # clients are defined in the config.yaml file
    
      $ dxp build_gui   # FIXME: broken...
      $ dxp run_gui     # FIXME: broken...
    
        
    
    positional arguments:
      {version,clean_homedir,clean,build,build_gui,run,run_cli,run_gui,list,monitor,deploy,deploy_node,truncate,feed_publish,feed_fetch,install_boost}
                            the command to run
      environment           the build/run environment (dxp, steem, ...)
      args                  additional arguments to be passed to the given command
    
    optional arguments:
      -h, --help            show this help message and exit
      -p PIDFILE, --pidfile PIDFILE
                            filename in which to write PID of child process
      -f, --forward-signals
                            forward unix signals to spawned blockproducer client child process
    
    You should also look into ~/.dxp_tools/config.yaml to tune it to your liking.
    



Building and running the DxpChain command-line client
------------------------------------------------------

To build and run the command-line client, you can use the following two commands::

    $ dxp build
    $ dxp run

By default, ``dxp build`` will build the latest version of the DxpChain client
(available on the master branch). If you want to build a specific version, you
can do so by specifying either the tag or the git hash.::

    $ dxp build 0.5.3
    $ dxp build 8c908f8

After the command-line client is successfully built, it will be installed in
the ``bin_dir`` directory as defined in the ``build_environments`` section of the
``config.yaml`` file. The last built version will also be symlinked as
``blockproducer_node`` in that directory, and this is the binary that a call
to ``dxp run`` will execute.

You can see a list of all binaries available by typing::

    $ dxp list


Passing additional arguments to "dxp run"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can pass additional arguments to "dxp run" and the tools will forward them
to the actual invocation of the dxp client. This can be useful for options that
you only use from time to time, eg: re-indexing the blockchain, or clearing the
peer database. If they are args that start with a double dash (eg: --my-option),
then you need to also prepend those with an isolated double dash, ie::

    $ dxp run -- --resync-blockchain --clear-peer-database

otherwise, the "--resync-blockchain" and "--clear-peer-database" would be
considered to be an option for the dxp script, and not an argument that should
be forwarded.


Building and running the DxpChain GUI client
---------------------------------------------

[FIXME: currently broken]

To build and run the GUI client, the procedure is very similar to the one for
the command-line client::

    $ dxp build_gui
    $ dxp run_gui

There is one major difference though: the GUI client will not be installed
anywhere and will always be run from the build directory. This is done so in
order to be as little intrusive as possible (ie: not mess with a wallet you
already have installed) and as install procedures are not as clear-cut as for
the command-line client.
