DxpChain delegate tools
------------------------

The DXP Tools will help you build, run and monitor any Graphene-based client
(currently DxpChain, Steem, Muse, PeerPlays).

There are 2 tools currently provided:

- command line utility allowing to quickly build and run any graphene-based client
- web application allowing to monitor a running instance of the client and send
  an email or push notification on failure

If you like these tools, please vote for
[blockproducer wackou](https://steemit.com/blockproducer-category/@wackou/wackou-blockproducer-post)
on the Steem, DxpChain and Muse networks. Thanks!


Documentation
=============

The main documentation for the tools, as well as a tutorial, can be found
on [ReadTheDocs](http://dxp-tools.readthedocs.io/).


Command-line client
===================

just run the ``dxp`` script with the command you want to execute:

    $ dxp -h
    usage: dxp [-h] [-p PIDFILE] [-f]
               {version,clean_homedir,clean,build,build_gui,run,run_cli,run_gui,list,monitor,deploy,deploy_node}
               [environment] [args [args ...]]
    
    following commands are available:
      - version                : show version of the tools
      - clean_homedir          : clean home directory. WARNING: this will delete your wallet!
      - save_blockchain_dir    : save a snapshot of the current state of the blockchain
      - restore_blockchain_dir : restore a snapshot of the current state of the blockchain
      - clean                  : clean build directory
      - build                  : update and build dxp client
      - build_gui              : update and build dxp gui client
      - run                    : run latest compiled dxp client, or the one with the given hash or tag
      - run_cli                : run latest compiled dxp cli wallet
      - run_gui                : run latest compiled dxp gui client
      - list                   : list installed dxp client binaries
      - monitor                : run the monitoring web app
      - deploy                 : deploy built binaries to a remote server
      - deploy_node            : full deploy of a seed or blockproducer node on given ip address. Needs ssh root access
    
    Examples:
      $ dxp build                 # build the latest dxp client by default
      $ dxp build v0.4.27         # build specific version
      $ dxp build ppy-dev v0.1.8  # build a specific client/version
      $ dxp run                   # run the latest compiled client by default
      $ dxp run seed-test         # clients are defined in the config.yaml file
    
      $ dxp build_gui   # FIXME: broken...
      $ dxp run_gui     # FIXME: broken...
    
    
    
    positional arguments:
      {version,clean_homedir,clean,build,build_gui,run,run_cli,run_gui,list,monitor,deploy,deploy_node}
                            the command to run
      environment           the build/run environment (dxp, steem, ...)
      args                  additional arguments to be passed to the given command
    
    optional arguments:
      -h, --help            show this help message and exit
      -p PIDFILE, --pidfile PIDFILE
                            filename in which to write PID of child process
      -f, --forward-signals
                            forward unix signals to spawned blockproducer client child process
    
    You should also look into ~/.dxp_tools/config.yaml to tune it to your liking.


Monitoring web app
==================

To run the debug/development monitoring web app, just do the following:

    $ dxp monitor
    
and it will launch on ``localhost:5000``.

For production deployments, it is recommended to put it behind a WSGI server, in which case the
entry point is ``dxp_tools.wsgi:application``.

Do not forget to edit the ``~/.dxp_tools/config.yaml`` file to configure it to suit your needs.
     

### Screenshots ###

You can see a live instance of the dxp tools monitoring the state of the
seed nodes I am making available for the DxpChain, Muse and Steem networks
here: http://seed.steemnodes.com
