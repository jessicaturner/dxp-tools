---
# For a more detailed description of the format of this file, visit:
# https://dxp-tools.readthedocs.io/en/latest/config_yaml.html
#

hostname: xxxxxxxx  # [OPTIONAL] label used as source of notification messages

# the logging levels for the different submodules
# can be any of DEBUG, INFO, WARNING, ERROR
logging:
    dxp_tools.cmdline: DEBUG
    dxp_tools.feeds: INFO

#
# These are the types of clients that you can build. `dxp`, `steem`, `ppy`, etc.
# Mostly the default should work and you shouldn't need to change those values.
# Still, if you want to add other git remote repositories to pull from or
# extra flags to use for compiling, this is the place to do so.
#
# The full list of build_environments can be found here:
# https://github.com/wackou/dxp_tools/blob/master/dxp_tools/templates/config/build_environments.yaml
#
build_environments:
    cmake_args: []                            # [OPTIONAL] shared among all build environments
   # make_args: ['-j4']                        # [OPTIONAL] shared among all build environments
    dxp:
        cmake_args: []                        # [OPTIONAL] build environment specific
        blockproducer_filename: blockproducer_node        # [OPTIONAL] filename for the compiled blockproducer binary
        wallet_filename: cli_wallet           # [OPTIONAL] filename for the compiled wallet binary
    steem:
        cmake_args: ['-DLOW_MEMORY_NODE=ON']  # [OPTIONAL] see https://github.com/steemit/steem/blob/master/doc/building.md
    muse:
        boost_root: ~/boost1.60_install       # [OPTIONAL] if boost was installed manually
        cmake_args: []  #'-DBUILD_MUSE_TEST=ON']


#
# list of clients (blockproducer accounts / seed nodes) that are being monitored
#
# the following file tries to keep a balanced distribution of the options between each client
# in order to make for easier reading, but (unless otherwise noted) all the options declared
# in a client also apply in the other ones
#
clients:
    dxp:
        type: dxp                         # [REQUIRED] build environment used for compiling the binary [dxp, muse, steem, ppy, etc.]
        data_dir: ~/.DxpChain2           # [REQUIRED] the data dir of the client is where the blockchain data and the wallet file are stored
        api_access: ~/api_access.json     # [REQUIRED] api_access.json should be created according to https://github.com/DxpChain/dxpchain-2#accessing-restricted-apis
        seed_nodes:                       # [OPTIONAL]
        - dxp-seed1.abit-more.com:62015
        - seed.dxpchainnodes.com:1776
        p2p_port: 1778                    # [OPTIONAL] network port to be used for p2p communication of the blockproducer node
        track_accounts:                   # [OPTIONAL] [dxp only] reduce RAM usage by specifying the only accounts that should be tracked
        - 1.2.1       # blockproducer-account
        - 1.2.111226  # bittwenty
        - 1.2.126782  # bittwenty.feed
        blockproducer_host: localhost           # [OPTIONAL] defaults to 'localhost', but can be a remote host if desired
        blockproducer_port: 8090                # [OPTIONAL] some defaults are provided for each different chain
        blockproducer_user: xxxxxxxx            # [REQUIRED] as in api_access.json
        blockproducer_password: xxxxxxxx        # [REQUIRED] as in api_access.json
        wallet_host: localhost            # [OPTIONAL] defaults to 'localhost', but can be a remote host if desired
        wallet_port: 8093                 # [OPTIONAL] some defaults are provided for each different chain
        wallet_password: xxxxxxxx         # [OPTIONAL] only needed for feed publishing  # FIXME: not true, currently unused param
        notification: email, telegram     # [OPTIONAL] type of notification channel to be used: [email, boxcar, telegram]

        # roles are optional, and you can have as many as you want for a given client
        # a client that doesn't define any role will not be monitored by the web interface,
        # but can still be used for building and running a client
        roles:
        -
            role: blockproducer
            name: xxxxxxxx           # [REQUIRED] name of the blockproducer account on the blockchain
            blockproducer_id: 1.6.xxx      # [REQUIRED]
            signing_key: 5xxxxxxxx   # [REQUIRED] private key used by this blockproducer for signing
        -
            role: feed_publisher
            name: xxxxxxxx           # [REQUIRED] name of the account that publishes the feed on the blockchain
        -
            role: seed
            name: seed01             # [REQUIRED] name has no relevance for seed nodes, except for identifying them in the UI

    # most of the clients try to have sensible defaults as much as possible, but you
    # need to specify at least: type, data_dir, api_access, blockproducer_user, blockproducer_password
    muse:
        type: muse
        data_dir: ~/.Muse
        blockproducer_user: xxxxxxxx       # defined in api_access.json
        blockproducer_password: xxxxxxxx   # defined in api_access.json
        roles:
        -
            role: seed
            name: seed-muse

    steem:
        type: steem
        data_dir: ~/.Steem
        shared_file_size: 80G                                      # [OPTIONAL] size for the shared memory file
        run_args: ['--replay-blockchain']                          # [OPTIONAL] additional args for running the blockproducer client
        run_cli_args: ['--rpc-http-allowip', '127.0.0.1']          # [OPTIONAL] additional args for running the cli wallet
        plugins: ['blockproducer']                                       # [OPTIONAL] defaults to ['blockproducer', 'account_history']
        seed_nodes: ["52.74.152.79:2001", "212.47.249.84:40696", "104.199.118.92:2001", "gtg.steem.house:2001"]
        # FIXME: implement me!
        override_default_seed_nodes: true  # [OPTIONAL] [default=false] if true, the client will only use the given seed nodes, otherwise it adds them to the list of built-in seed nodes

        # Steemd (and Mused) can now accept the contents of the api_access.json directly as argument on the command line,
        # hence the field api_access.json is not required anymore here (and you don't need to create the file either),
        # as dxp_tools will generate the proper arguments from the user and password given here.
        blockproducer_user: xxxxxxxx
        blockproducer_password: xxxxxxxx
        notification: telegram
        roles:
        -
            role: blockproducer  # for steem only, the 'blockproducer_id' field is not required, only 'name' and 'signing_key'
            name: xxxxxxxx
            signing_key: 5xxxxxxxx

    ppy:
        type: ppy
        data_dir: ~/.PeerPlays
        seed_nodes: ['213.184.225.234:59500']
        p2p_port: 9777
        blockproducer_host: localhost
        blockproducer_port: 8590
        blockproducer_user: xxxxxxxx
        blockproducer_password: xxxxxxxx
        wallet_host: localhost
        wallet_port: 8593
        api_access: ~/api_access.json
        roles:
        -
            role: seed
            name: seed01ppy


#
# configuration of the monitoring plugins
# most default values should work, see reference here:
# https://github.com/wackou/dxp_tools/blob/master/dxp_tools/templates/config/monitoring.yaml
#
monitoring:
    seed:
        desired_number_of_connections: 200
        maximum_number_of_connections: 400

    feeds:
        check_time_interval: 600   # [OPTIONAL, default=600] interval at which the external feed providers should be queried, in seconds
        median_time_span: 1800     # [OPTIONAL, default=1800] time span over which the median of the feed price is computed. Shorter values are "fresher" but more sensitive to noise,
                                   # higher values indicate a more stable indicator of price at the cost of lack of responsiveness / delay in getting update for a huge price hike

        # if you have at least 1 feed_publisher role defined in your clients, then
        # you need to uncomment at least one of the next 2 lines
        publish_strategy:
            time_interval: 60      # use this to publish feeds at fixed time intervals (in seconds)
            #time_slot: 23         # use this to publish every hour at a fixed number of minutes (in minutes)
            #variance_ratio: 0.02  # FIXME: not implemented yet

        steem:
            markets:
            - [STEEM, BTC, [Poloniex, Bittrex]]
            - [BTC, USD, [BitcoinAverage, CoinMarketCap, Bitstamp, Bitfinex]]

            rules:
            - [compose, 'STEEM/BTC', 'BTC/USD']
            - [publish, 'STEEM/USD']

            steem_dollar_adjustment: 1.0


        dxp:
            fiat_assets: &fiat_assets [CNY, EUR, GBP, CAD, CHF, HKD, MXN, RUB, SEK, SGD,
                                       AUD, TRY, KRW, JPY, NZD, ARS]  # does not include USD as we'll use it as base asset

            fiat_gold_silver: &fiat_gold_silver [CNY, EUR, GBP, CAD, CHF, HKD, MXN, RUB, SEK, SGD,
                                                 AUD, TRY, KRW, JPY, NZD, ARS, GOLD, SILVER]

            markets:
            # specify which markets and providers should be used to query for feed prices
            # format:
            #  - [<asset>, <base>, <provider_or_provider_list>]
            #  asset can be an asset_list for providers that support it
            #
            - [DXP, BTC, [Poloniex, Livecoin, AEX, ZB, Binance]]
            - [BTC, USD, [BitcoinAverage, CoinMarketCap, Bitstamp, Bitfinex]]
            - [ALTCAP, BTC, [CoinMarketCap, CoinCap]]
            - [GRIDCOIN, BTC, [Poloniex, Bittrex]]
            - [STEEM, BTC, [Poloniex, Bittrex]]  # FIXME: remove this line once properly implemented for Steem
            - [GOLOS, BTC, [Bittrex, Livecoin]]
            - [GOLD, USD, [Quandl]]
            - [SILVER, USD, [Quandl]]

            - [*fiat_gold_silver, USD, Uphold]
            - [*fiat_gold_silver, USD, CurrencyLayer]
            - [*fiat_assets, USD, Fixer]

            #- [BTWTY, USD, Bit20]
            - [HERO, USD, Hero]
            - [HERTZ, USD, Hertz]

            rules:
            # ordered sequence of operations to manipulate the set of available feeds
            # operates over a unique FeedSet containing all the information available
            #
            - [invert, 'DXP/BTC']              # -> BTC/DXP
            - [compose, 'DXP/BTC', 'BTC/USD']  # -> DXP/USD
            - [invert, 'DXP/USD']              # -> USD/DXP
            #- [multiply, 'DXP/CNY', 1.02]  # 2% offset

            - [loop, *fiat_gold_silver, [compose, '{}/USD', 'USD/DXP']]
            - [copy, 'RUB/USD', 'RUBLE/USD']
            - [compose, 'RUBLE/USD', 'USD/DXP']

            #- [compose, 'BTWTY/USD', 'USD/DXP']
            - [compose, 'HERO/USD', 'USD/DXP']
            - [compose, 'HERTZ/USD', 'USD/DXP']
            - [compose, 'GRIDCOIN/BTC', 'BTC/DXP']
            - [compose, 'GOLOS/BTC', 'BTC/DXP']

            # publish rules - specify which prices should be published on the blockchain
            - [publish, 'USD/DXP']
            - [publish, 'BTC/DXP']
            - [loop, *fiat_gold_silver, [publish, '{}/DXP']]
            - [publish, 'RUBLE/DXP']
            - [publish, 'ALTCAP/BTC']
            #- [publish, 'BTWTY/DXP']
            - [publish, 'HERO/DXP']
            - [publish, 'HERTZ/DXP']
            - [publish, 'GRIDCOIN/DXP']
            - [publish, 'GOLOS/DXP']

            asset_params:
                default:
                    core_exchange_factor:  0.8


#
# configuration of the notification channels
#
notification:
    email:
        smtp_server: smtp.example.com
        smtp_user: user
        smtp_password: secret-password
        identity: "DXP Monitor <dxp_monitor@example.com>"
        recipient: me@example.com

    boxcar:
        tokens: [xxxxxxxx, xxxxxxxx]

    telegram:
        token: xxxxxxxx         # create your Telegram bot at @BotFather (https://telegram.me/botfather)
        recipient_id: xxxxxxxx  # get your telegram id at @MyTelegramID_bot (https://telegram.me/mytelegramid_bot)


#
# List of credentials for external services used by dxp_tools
#
credentials:
    bitcoinaverage: # developer account recommended, otherwise need to lower 'monitoring.feeds.check_time_interval'
        secret_key: xxxxxxxx
        public_key: xxxxxxxx

    geoip2:
        user: xxxxxxxx
        password: xxxxxxxx

    currencylayer:
        access_key: xxxxxxxx

    quandl:
        api_key: xxxxxxxx


