# -*- coding: utf-8 -*-

# import this module using
# >>> import dxp_tools.repl
# to set up a (dummy) flask context to allow you to access the DB

# from dxp_tools.wsgi import application
# ctx = application.app.test_request_context()
#
# import dxp_tools.rpcutils as rpc
# import dxp_tools.core as core
#
# ctx.push()

import dxp_tools

dxp_tools.init()
