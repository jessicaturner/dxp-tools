#!/usr/bin/env bash

echo "CALLING install_user.sh WITH ARGS: $@"

set -o nounset
USER="{{ unix_user }}"
GIT_NAME="{{ git['name'] }}"
GIT_EMAIL="{{ git['email'] }}"
set +o nounset

cd ~

# install ssh keys file
if [ ! -f ~/.ssh/id_rsa ]; then
    ssh-keygen -b 4096 -N "" -f ~/.ssh/id_rsa
fi
if [ -f /tmp/authorized_keys ]; then
    cp /tmp/authorized_keys ~/.ssh/
fi

# install and activate virtualenvwrapper
cat <<EOF >> .bashrc

. /usr/share/virtualenvwrapper/virtualenvwrapper_lazy.sh
EOF

source /usr/share/virtualenvwrapper/virtualenvwrapper_lazy.sh

# clone git repo
if [ ! -d ~/dxp_tools ]; then
    git clone https://github.com/wackou/dxp_tools
fi
cd dxp_tools
git config user.email "$GIT_EMAIL"
git config user.name "$GIT_NAME"

# create virtualenv
echo "create dxp_tools virtualenv"
mkvirtualenv -p /usr/bin/python3 dxp_tools

echo "install git version of the dxp_tools"
pushd ~/dxp_tools

workon dxp_tools

# upgrade pip and setuptools to ensure a modern installation (and no problem when installing dependencies)
pip install -U pip setuptools

rm -fr dist; python setup.py sdist && (pip uninstall -y dxp_tools; pip install dist/dxp_tools-*.tar.gz)

# ensure we have a ~/.dxp_tools folder with default config.yaml
dxp list >/dev/null 2>&1


if [ -f /tmp/config.yaml ]; then
    # FIXME: unnecessary due to line 52, right?
    if [ -f ~/.dxp_tools/config.yaml ]; then
        echo "config yaml found in .dxp_tools"
    else
        echo "ERROR: still no config yaml in .dxp_tools"
    fi
    mkdir -p /home/$USER/.dxp_tools
    cp /tmp/config.yaml dxp_tools/config.yaml                    # copy config.yaml to local dev dir
    cp dxp_tools/config.yaml /home/$USER/.dxp_tools/config.yaml  # copy config.yaml to dxp_tools config dir
    echo "------------------------------------"
    echo "installed config.yaml"
    cat /home/$USER/.dxp_tools/config.yaml
    echo "------------------------------------"
else
    echo "no config.yaml file given"
fi

if [ -f /tmp/launch_tmux_sessions.sh ]; then
    cp /tmp/launch_tmux_sessions.sh ~
    chmod a+x ~/launch_tmux_sessions.sh
fi

{% if compile_on_new_host %}
# compile client locally

# install needed version(s) of boost first
    {%- set boost_required = set() -%}
    {%- for client in config_yaml['clients'] -%}
        {%- set client_type = config_yaml['clients'][client].get('type', client) -%}
        {%- set boost_root = config_yaml['build_environments'].get(client_type, {}).get('boost_root', None) -%}

        {%- if boost_root -%}
            {#- if boost_root is defined, install it first before compiling -#}
            {%- set boost_ver = re.findall('1.([0-9]+)', boost_root)[-1] -%}
            {%- set tmp = boost_required.add(boost_ver) -%}
        {%- endif -%}
    {%- endfor -%}

    {% for boost_ver in boost_required %}
dxp install_boost {{ boost_ver }}
    {% endfor %}

# compile all the various clients
{% for client in config_yaml['clients'] -%}
        {%- set client_type = config_yaml['clients'][client].get('type', client) -%}

echo "Building {{ client_type }} client for {{ client }}"
dxp build {{ client_type }}

{% endfor %}
{% else %}

echo "Not building client locally"

{% endif %}

# copy api_access.json files, if given
# try copying any genesis file also in the home directory
cp /tmp/*.json ~/

popd
